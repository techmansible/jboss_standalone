#!/bin/bash

function print_help {
  cat <<EOF
=====

$ ./destroy.sh [--extravars|-v] variables

Example:

$ ./destroy.sh -v vars/ev-thlifeuwny002.yml

=====
EOF
}

if [ $# -ne 2 ]; then
  print_help
else
  for i in $@; do
    case $1 in
      --extravars|-v)
      _extravars=$(realpath $2)
      ;;
      *)
      print_help
      exit 1
    esac
  done

  dt=$(date +%Y%m%d%H%M)
  _passfile="/var/tmp/.$(head /dev/urandom | tr -dc A-Za-z | head -c 8 ; echo '')"
  time ansible-playbook -e "vars=$_vars" -e "azure_vault_passfile=$_passfile" -e "@cyberark-config.yml" -e "state=present" -e "no_log_status=True" azure-vault-password.yml -v | tee logs/output.${dt}.log
  time ansible-playbook -i inventory -e "@$_extravars" -e "@cyberark-config.yml" -e "state=absent" -e "no_log_status=True" --vault-password-file=$_passfile destroy.yml -v | tee -a logs/output.${dt}.log
  ansible-playbook -e "vars=$_vars" -e "azure_vault_passfile=$_passfile" -e "@cyberark-config.yml" -e "state=absent" -e "no_log_status=True" azure-vault-password.yml
fi
