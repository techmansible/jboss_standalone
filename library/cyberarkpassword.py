#!/usr/bin/env python

from ansible.module_utils.basic import *
import argparse
import struct
import subprocess
import sys
import urlparse
import os
from ntlm3 import HTTPNtlmAuthHandler
import urllib2

def retrievePassword(data,module):
    isError = False
    hasChanged = True
    meta = {}
    resp = {
           "status": "Error",
           "response" : {},
           "error": "",
           "hasChanged": False,
           "isError": True
    }
    try:
      isError = False
      hasChanged = False
      password_struct = get(data, resp, module)
      resp['response'] = password_struct
      meta = buildResponse(resp,{},hasChanged,isError,module)
    except Exception as e:
      isError = True
      hasChanged = False
      resp['error'] =  'Something failed while retrieving password: %s' % str(e)
      meta =  buildResponse(resp,str(e),hasChanged,isError,module)
      module.fail_json(**meta)
    return isError, hasChanged, meta

def buildResponse(result, err, hasChanged, isError, module):
    print "Inside buildResponse().\n"
    result["status"] = "OK"
    result["hasChanged"] = hasChanged
    result["isError"] = isError
    return result


def get(data, resp, module):
    resultDict = {}

    try:
        allParms = [
            "/opt/CARKaim/sdk/clipasswordsdk",
            "GetPassword",
            "-p",
            "AppDescs.AppID=" + data['app_id'],
            "-p",
            "Query=" + "safe=" + data['safe'] + ";folder=" + data['folder'] + ";object=" + data['objectid'],
            "-o",
            data['output'],
            "-d",
            data['delimiter']
        ]

        credential = subprocess.check_output(allParms)

        if len(credential) > 0 and credential.endswith("\n"):
                credential = credential[:-1]

        outputNames = data['output'].split(",")
        outputValues = credential.split(data['delimiter'])

        for i in range(len(outputNames)):
            if outputNames[i].startswith("passprops."):
                if "passprops" not in resultDict:
                    resultDict["passprops"] = {}
                outputPropName = outputNames[i][10:]
                resultDict["passprops"][outputPropName] = outputValues[i]
            else:
                resultDict[outputNames[i]] = outputValues[i]

    except Exception as e:
        resp['error'] = "Error occured during clipasswordsdk call: %s" % str(e) 
        module.fail_json(**meta)
        raise e
    except OSError as e:
        resp['error'] = "Error occured during clipasswordsdk call: %s" % str(e)
        module.fail_json(**meta)
        raise Exception("ERROR - AIM not installed or clipasswordsdk not in standard location. ERROR=(" + str(e.errno) + ") => " + e.strerror)
    return [resultDict]

def main():
    fields = {
        "app_id": {
            "required": False,
            "type": "str",
            "default": "app_ansible"
        },
        "safe": {
            "required": False,
            "type": "str",
            "default": "NGDC-Ansible"
        },
        "folder": {
            "required": False,
            "type": "str",
            "default": "root"
        },
        "output": {
            "required": False,
            "type": "str",
            "default": "Password,PassProps.UserName,PassProps.Address,PasswordChangeInProcess"
        },
        "objectid": {
            "required": True,
            "type": "str",
        },
        "delimiter": {
            "required": False,
            "type": "str",
            "default":"#|#"
        },
        "state": {
            "required": False,
            "type": "str",
            "default":"retrieved"
        }
    }
    choice_map = {
        "retrieve": retrievePassword,
    }
    module = AnsibleModule(argument_spec=fields)
    is_error, has_changed, result = choice_map.get(
        module.params['state'])(module.params, module)

    if not is_error:
        module.exit_json(changed=has_changed, meta=result)
    else:
        module.fail_json(msg="An error occured while retrieving password from CyberArk.", meta=result)
if __name__ == '__main__':
    main()
