#!/bin/bash

function print_help {
  cat <<EOF
=====

$ ./master-playbook.sh [--extravars|-v] variables

Example:

$ ./master-playbook.sh -v vars/ev-thlifeuwny002.yml

=====
EOF
}

if [ $# -ne 2 ]; then
  print_help
else
  for i in $@; do
    case $1 in
      --extravars|-v)
      _extravars=$(realpath $2)
      ;;
      *)
      print_help
      exit 1
    esac
  done

  ansible-playbook -i inventory -e "@$_extravars" -e "@cyberark-config.yml" -e "state=present" master-playbook.yml -v | tee -a logs/output.${dt}.log
fi
