import random
import string

def is_forbidden(x):
    forbidden_chars = "#0oO|Il';\\"
    return forbidden_chars.find(x)!=-1

def passgen():
    length = 8
    pwd = []
    ch = "x"

    forb = True
    while forb:
        ch = random.choice(string.ascii_lowercase)
        forb = is_forbidden(ch)
    pwd.append(ch)

    forb = True
    while forb:
        ch = random.choice(string.ascii_uppercase)
        forb = is_forbidden(ch)
    pwd.append(ch)

    forb = True
    while forb:
        ch = random.choice(string.ascii_letters)
        forb = is_forbidden(ch)
    pwd.append(ch)

    forb = True
    while forb:
        ch = random.choice(string.ascii_letters)
        forb = is_forbidden(ch)
    pwd.append(ch)

    forb = True
    while forb:
        ch = random.choice(string.ascii_letters)
        forb = is_forbidden(ch)
    pwd.append(ch)

    forb = True
    while forb:
        ch = str(random.randint(1,9))
        forb = is_forbidden(ch)
    pwd.append(ch)

    forb = True
    while forb:
        ch = str(random.randint(1,9))
        forb = is_forbidden(ch)
    pwd.append(ch)

    forb = True
    while forb:
        #ch = random.choice(string.punctuation)
        ch = random.choice([',', '.', '_', '+', ':', '@', '%', '/'])
        forb = is_forbidden(ch)
    pwd.append(ch)

    random.shuffle(pwd)
    return ''.join(pwd)

if __name__ == "__main__":
    print passgen()
