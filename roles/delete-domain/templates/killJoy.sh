#!/bin/bash
ps -ef | grep java | grep ${PROCESS_USER} | grep -v bash | awk '{print $2}' | awk '{printf("kill -9 %s\n", $1)}' | /bin/bash
rm -Rf ${JBOSS_HOME}
rm -Rf ${DOMAIN_HOME}
