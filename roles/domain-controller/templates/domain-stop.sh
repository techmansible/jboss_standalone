#!/bin/bash
#This script stops the JBoss EAP 7.x domain controller for this environment.

setup()
{
  JAVA_HOME={{ mjdkHome }}
  PATH=${JAVA_HOME}/bin:${PATH}
  JBOSS_HOME={{ mjbossHome }}
  DOMAIN_CONFIG={{ mdomainConfigName }}
  HOST_CONFIG={{ mhostConfigName }}
  MGMT_HOST={{ mmgmtHost }}
  MGMT_PORT={{ msecureMgmtPort }}
  TRUST_STORE={{ mmasterTrustStorePath }}
  DOMAIN_HOME={{ mdomainHome }}
  HOST_NAME={{ mmasterHostName }}
}

stopWithCredentials()
{
  ${JBOSS_HOME}/bin/jboss-cli.sh --connect \
   --controller=https-remoting://${MGMT_HOST}:${MGMT_PORT} \
   --user=${ADMIN_USER} \
   --password=${ADMIN_PASSWORD} \
   -Djavax.net.ssl.trustStore=${TRUST_STORE} \
   --command=/host=${HOST_NAME}:shutdown
  rc=$?
  if [ $rc -ne 0 ];
  then
    echo "An error occured during shutdown.  See previous messages."
    exit 1
  fi
}

#this must be run from the local machine as the jboss user
stopWithoutCredentials()
{
  ${JBOSS_HOME}/bin/jboss-cli.sh --connect --command=/host=${HOST_NAME}:shutdown
  rc=$?
  if [ $rc -ne 0 ];
  then
    echo "An error occured during shutdown.  See previous messages."
    exit 1
  fi
}

setup
LOCAL_AUTH=false
while getopts "hlu:p:" opt; do
  case ${opt} in
    h ) echo "DOMAIN-domain-stop.sh [ -l ]"
        ;;
    l ) LOCAL_AUTH=true 
        ;;
    u ) ADMIN_USER=$OPTARG
        ;;
    p ) ADMIN_PASSWORD=$OPTARG
        ;;
    * ) echo "Invalid option." 
      ;;
  esac
done

if [ -z "${ADMIN_USER}" ] && [ "${LOCAL_AUTH}" = "false" ];
then
  echo -n "Enter Admin username:"
  read ADMIN_USER
fi

if [ -z "${ADMIN_PASSWORD}" ] && [ "${LOCAL_AUTH}" = "false" ];
then
  echo -n "Enter Admin password:"
  read -s ADMIN_PASSWORD
fi

if [ "${LOCAL_AUTH}" = "true" ];
then
  stopWithoutCredentials
else
  stopWithCredentials
fi
exit 0
