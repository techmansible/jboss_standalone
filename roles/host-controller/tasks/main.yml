---
- name: "Setting targetHost to be this host: {{ inventory_hostname }}"
  set_fact:
    targetHost: "{{ item }}"
  with_items:
    - "{{ slaveInterface.keys() }}"
  when: slaveInterface[item] == inventory_hostname

- name: Does JBoss EAP install path exist?
  stat:
    path: "{{ jbossHome[targetHost] | default(defaults[env_].jbossHome) }}"
  register: command_output
  notify: printResults

- name: Delete domain directory, if present
  file:
    path: "{{ domainHome[targetHost] | default(defaults[env_].domainHome) }}"
    state: absent
  become: yes
  become_user: root
  register: command_output
  notify: printResults

- name: Create domain directory
  file:
   path: "{{ domainHome[targetHost] | default(defaults[env_].domainHome) }}"
   state: directory
   recurse: yes
   owner: "{{ mprocessUser }}"
   group: "{{ mprocessUser }}"
  become: yes
  become_user: root
  register: command_output
  notify: printResults

- name: "Create {{ domainHome[targetHost] | default(defaults[env_].domainHome) }}/data"
  file:
   path: "{{ domainHome[targetHost]  | default(defaults[env_].domainHome) }}/data"
   state: directory
   recurse: yes
  register: command_output
  notify: printResults

- name: "Create {{ domainHome[targetHost]  | default(defaults[env_].domainHome) }}/tmp"
  file:
   path: "{{ domainHome[targetHost] | default(defaults[env_].domainHome) }}/tmp"
   state: directory
   recurse: yes
  register: command_output
  notify: printResults

- name: "Create {{ domainHome[targetHost] | default(defaults[env_].domainHome) }}/servers"
  file:
   path: "{{ domainHome[targetHost] | default(defaults[env_].domainHome) }}/servers"
   state: directory
   recurse: yes
  register: command_output
  notify: printResults

- name: "Create {{ domainHome[targetHost] | default(defaults[env_].domainHome) }}/configuration"
  file:
   path: "{{ domainHome[targetHost] | default(defaults[env_].domainHome) }}/configuration"
   state: directory
   recurse: yes
  register: command_output
  notify: printResults

- name: "Create {{ domainHome[targetHost] | default(defaults[env_].domainHome) }}/log"
  file:
   path: "{{ domainHome[targetHost] | default(defaults[env_].domainHome) }}/log"
   state: directory
   recurse: yes
  register: command_output
  notify: printResults

- name: "Create {{ domainHome[targetHost] | default(defaults[env_].domainHome) }}/bin"
  file:
   path: "{{ domainHome[targetHost] | default(defaults[env_].domainHome) }}/bin"
   state: directory
   recurse: yes
  register: command_output
  notify: printResults

- name: Get file names to copy for default configuration files
  command: "find {{ jbossHome[targetHost]  | default(defaults[env_].jbossHome) }}/domain/configuration/ -type f"
  register: files_to_copy

- name: copy default configuration files
  copy:
    src: "{{ item }}"
    dest: "{{ domainHome[targetHost]  | default(defaults[env_].domainHome) }}/configuration/"
    owner: "{{ mprocessUser }}"
    group: "{{ mprocessUser }}"
    remote_src: True
    mode: 0600
    follow: yes
  become: yes
  become_user: root
  with_items:
   - "{{ files_to_copy.stdout_lines }}"

- name: Create default domain config file for this domain by copying domain.xml.
  command: "cp {{ baseDomainConfig[targetHost]  | default(defaults[env_].baseDomainConfig) }} {{ domainConfig[targetHost] | default(defaults[env_].domainConfig) }}"
  register: command_output
  notify: printResults

- name: Create default host config file for this domain by copying host*.xml.
  command: "cp {{ baseHostConfig[targetHost] | default(defaults[env_].baseHostConfig) }} {{ hostConfig[targetHost] | default(defaults[env_].hostConfig) }}"
  register: command_output
  notify: printResults

- name: "Rename host in {{ hostConfig[targetHost] | default(defaults[env_].hostConfig) }}."
  oxml:
    path: "{{ hostConfig[targetHost] | default(defaults[env_].hostConfig) }}"
    xpath: /xmlns:host
    namespaces:
      xmlns: "{{ nsjbossUri }}"
    attribute: name
    value: "{{ slaveHostName[targetHost] | default(defaults[env_].slaveHostName) }}"
  register: command_output
  notify: printResults

- name: "Remove default server groups from { domainConfig[targetHost] | default(defaults[env_].domainConfig) }}."
  xml:
    path: "{{ domainConfig[targetHost] | default(defaults[env_].domainConfig) }}"
    xpath: //xmlns:server-groups/*
    namespaces:
      xmlns: "{{ nsjbossUri }}"
    ensure: absent
  register: command_output
  notify: printResults

- name: "Remove server definitions from {{ hostConfig[targetHost]  | default(defaults[env_].hostConfig) }}."
  xml:
    path: "{{ hostConfig[targetHost] | default(defaults[env_].hostConfig) }}"
    xpath: //xmlns:servers/*
    namespaces:
      xmlns: "{{ nsjbossUri }}"
    ensure: absent
  register: command_output
  notify: printResults

- name: "Add app server username to { hostConfig[targetHost] | default(defaults[env_].hostConfig) }}."
  xml:
    path: "{{ hostConfig[targetHost] | default(defaults[env_].hostConfig) }}"
    xpath: //xmlns:domain-controller/xmlns:remote
    namespaces:
      xmlns: "{{ nsjbossUri }}"
    attribute: username
    value: "{{ hostUsername[targetHost] | default(defaults[env_].hostUsername) }}"
  register: command_output
  notify: printResults

- name: "Add server-identities element to { hostConfig[targetHost] | default(defaults[env_].hostConfig) }}."
  xml:
    path: "{{ hostConfig[targetHost] | default(defaults[env_].hostConfig) }}"
    xpath: /xmlns:host/xmlns:management/xmlns:security-realms/xmlns:security-realm[ @name='ManagementRealm' ]/xmlns:server-identities
    namespaces:
      xmlns: "{{ nsjbossUri }}"
    ensure: present
  register: command_output
  notify: printResults

- name: "Remove existing server-identities secret element to { hostConfig[targetHost] | default(defaults[env_].hostConfig) }}."
  xml:
    path: "{{ hostConfig[targetHost] | default(defaults[env_].hostConfig) }}"
    xpath: /xmlns:host/xmlns:management/xmlns:security-realms/xmlns:security-realm[ @name='ManagementRealm' ]/xmlns:server-identities/xmlns:secret
    namespaces:
      xmlns: "{{ nsjbossUri }}"
    ensure: absent
  register: command_output
  notify: printResults

- name: "Add server-identities secret value element to { hostConfig[targetHost] | default(defaults[env_].hostConfig) }}."
  xml:
    path: "{{ hostConfig[targetHost] | default(defaults[env_].hostConfig) }}"
    xpath: /xmlns:host/xmlns:management/xmlns:security-realms/xmlns:security-realm[ @name='ManagementRealm' ]/xmlns:server-identities/xmlns:secret
    namespaces:
      xmlns: "{{ nsjbossUri }}"
    ensure: present
  register: command_output
  notify: printResults

- name: "Add value attribute to secret element { hostConfig[targetHost] | default(defaults[env_].hostConfig) }}."
  xml:
    path: "{{ hostConfig[targetHost] | default(defaults[env_].hostConfig) }}"
    xpath: /xmlns:host/xmlns:management/xmlns:security-realms/xmlns:security-realm[ @name='ManagementRealm' ]/xmlns:server-identities/xmlns:secret
    namespaces:
      xmlns: "{{ nsjbossUri }}"
    attribute: value
    value: "${VAULT::vb::serverAuthenticationPassword::1}"
  register: command_output
  notify: printResults

- name: "Initialize password vault."
  include_role:
    name: password-vault
  vars:
    pwvVaultKeystore: "{{ vaultKeystore[targetHost] | default(defaults[env_].vaultKeystore) }}"
    pwvVaultPassword: "{{ vaultPassword[targetHost] | default(defaults[env_].vaultPassword) }}"
    pwvVaultEncDir: "{{ vaultEncDir[targetHost] | default(defaults[env_].vaultEncDir) }}"
    pwvDomainHome: "{{ domainHome[targetHost] | default(defaults[env_].domainHome) }}"
    pwvJbossHome: "{{ jbossHome[targetHost]  | default(defaults[env_].jbossHome) }}"
    pwvVaultSaltIteration: "{{ vaultSaltIteration[targetHost]  | default(defaults[env_].vaultSaltIteration) }}"
    pwvVaultSalt: "{{ vaultSalt[targetHost] }}"
    pwvVaultKeystoreAlias: "{{ vaultKeystoreAlias[targetHost] | default(defaults[env_].vaultKeystoreAlias) }}"
    # pwvVaultPassword: "{{ vaultPassword[targetHost] }}"
    pwvCliTimeout: "{{ mcliTimeout }}"
    pwvMgmtIPAddr: "{{ mgmtIPAddr }}"
    pwvMgmtPort: "{{ mmgmtPort }}"
    pwvAdminUsername: "{{ madminADUsername }}"
    pwvAdminPassword:  "{{ madminADPassword }}"
    pwvHostName: "{{ mmasterHostName }}"
  register: command_output
  notify: printResults
  no_log: "{{ no_log_status }}"

- name: "Add LDAP bind password to password vault."
  include_role:
    name: add-password-2-vault
  vars:
    addPasswordJBossHome: "{{ jbossHome[targetHost] | default(defaults[env_].jbossHome) }}"
    addPasswordVaultEncDir: "{{ vaultEncDir[targetHost] | default(defaults[env_].vaultEncDir) }}"
    addPasswordVaultSaltIteration: "{{ vaultSaltIteration[targetHost] | default(defaults[env_].vaultSaltIteration) }}"
    addPasswordVaultKeystore: "{{ vaultKeystore[targetHost] | default(defaults[env_].vaultKeystore) }}"
    addPasswordVaultPassword: "{{ vaultPassword[targetHost] | default(defaults[env_].vaultPassword) }}"
    addPasswordVaultSalt: "{{ vaultSalt[targetHost] }}"
    addPasswordVaultKeystoreAlias: "{{ vaultKeystoreAlias[targetHost] | default(defaults[env_].vaultKeystoreAlias) }}"
    addPasswordKey: "ldapBindPassword"
    addPasswordValue: "{{ mldapbindPassword }}"
  register: command_output
  notify: printResults
  no_log: "{{ no_log_status }}"

- name: "Add server authentication password to password vault."
  include_role:
    name: add-password-2-vault
  vars:
    addPasswordJBossHome: "{{ jbossHome[targetHost] | default(defaults[env_].jbossHome) }}"
    addPasswordVaultEncDir: "{{ vaultEncDir[targetHost]  | default(defaults[env_].vaultEncDir) }}"
    addPasswordVaultSaltIteration: "{{ vaultSaltIteration[targetHost] | default(defaults[env_].vaultSaltIteration) }}"
    addPasswordVaultKeystore: "{{ vaultKeystore[targetHost]  | default(defaults[env_].vaultKeystore) }}"
    addPasswordVaultPassword: "{{ vaultPassword[targetHost] | default(defaults[env_].vaultPassword) }}"
    addPasswordVaultSalt: "{{ vaultSalt[targetHost] }}"
    addPasswordVaultKeystoreAlias: "{{ vaultKeystoreAlias[targetHost] | default(defaults[env_].vaultKeystoreAlias) }}"
    addPasswordKey: "serverAuthenticationPassword"
    addPasswordValue: "{{ hostPassword[targetHost] | default(defaults[env_].hostPassword) }}"
  register: command_output
  notify: printResults
  no_log: "{{ no_log_status }}"

- name: Add domain controller cert to local truststore.
  include: update-mgmt-truststore.yml

#Start Host Controller
- name: Start Host Controller
  include_role:
    name: start-host-controller
  vars:
  register: command_output
  notify: printResults

- name: Create management interface
  include_role:
    name: jboss-cli
  vars:
    jbossCliHome: "{{ jbossHome[targetHost] }}"
    jbossCliTimeout: "{{ mcliTimeout  }}"
    jbossCliHost: "{{ mgmtIPAddr }}"
    jbossCliPort: "{{ msecureMgmtPort }}"
    jbossCliAdminUser: "{{ madminADUsername }}"
    jbossCliAdminPassword: "{{ madminADPassword }}"
    jbossCliCommand: "/host={{ slaveHostName[targetHost] | default(defaults[env_].slaveHostName) }}/core-service=management/management-interface=http-interface/:add(console-enabled=false, interface=management, port=\"${jboss.management.http.port:9990}\", security-realm=ManagementRealm,http-upgrade-enabled=true)"
    jbossCliTrustStore: "{{ hostTrustStorePath[targetHost] | default(defaults[env_].hostTrustStorePath) }}"
  register: command_output
  notify: printResults
  no_log: "{{ no_log_status }}"

- name: Include start-scripts.yml
  include: start-scripts.yml
