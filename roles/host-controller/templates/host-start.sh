#!/bin/bash
#This script starts the JBoss EAP 7.x domain controller for this environment.
JAVA_HOME={{ jdkHome[targetHost] }}
PATH=${JAVA_HOME}/bin:${PATH}
JBOSS_HOME={{ jbossHome[targetHost] }}
DOMAIN_CONFIG={{ domainConfigName[targetHost] }}
HOST_CONFIG={{ hostConfigName[targetHost] }}
MGMT_HOST={{ mmgmtHost }}
MGMT_PORT={{ msecureMgmtPort }}
HOST_INTERFACE={{slaveInterface[targetHost] }}
DOMAIN_HOME={{ domainHome[targetHost] }}
HOST_USERNAME={{ hostUsername[targetHost] }}
STDOUT_LOG={{ hostStandardOutLog[targetHost] }}
STDERR_LOG={{ hostStandardErrLog[targetHost] }}
TRUST_STORE={{ hostTrustStorePath[targetHost] }}
export JAVA_OPTS="-Xms1024m -Xmx1024m -XX:MaxMetaspaceSize=256m -Djava.net.preferIPv4Stack=true -Djboss.modules.system.pkgs=org.jboss.byteman -Djava.awt.headless=true -verbose:gc -Xloggc:/opt/jboss/{{ mdomainHome }}/{{ slaveInterface[targetHost] }}/domain/log/gc.log -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=5 -XX:GCLogFileSize=3M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/opt/jboss/{{ mdomainHome }}/{{ slaveInterface[targetHost] }}/domain/log"
nohup ${JBOSS_HOME}/bin/domain.sh\
 --domain-config=${DOMAIN_CONFIG}\
 --host-config=${HOST_CONFIG}\
 -b ${HOST_INTERFACE}\
 -Djboss.domain.base.dir=${DOMAIN_HOME}\
 -Djboss.domain.master.address=${MGMT_HOST}\
 -Djboss.domain.master.port=${MGMT_PORT}\
 -Djboss.host.user.name=${HOST_USERNAME}\
 -Djboss.bind.address.management=${HOST_INTERFACE}\
 -Djboss.domain.master.protocol=https-remoting\
 -Djavax.net.ssl.trustStore=${TRUST_STORE}\
 -DHostManager\
 -secmgr\
 --backup\
 >${STDOUT_LOG}\
 2>${STDERR_LOG} &
exit 0
