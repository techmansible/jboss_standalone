#!/bin/bash
#This script starts the JBoss EAP 7.x domain controller for this environment.
{% for host_ in hostnames_.keys() %}
{% if host_ == targetHost  %}
JAVA_HOME={{ jdkHome[host_] }}
PATH=${JAVA_HOME}/bin:${PATH}
JBOSS_HOME={{ jbossHome[host_] }}
DOMAIN_CONFIG={{ domainConfigName[host_] }}
HOST_CONFIG={{ hostConfigName[host_] }}
HOST_NAME={{ slaveHostName[host_] }}
MGMT_HOST={{ mmgmtHost }}
MGMT_PORT={{ msecureMgmtPort }}
TRUST_STORE={{ hostTrustStorePath[host_] }}
SERVER_NAME=$1
{% endif %}
{% endfor %}

${JBOSS_HOME}/bin/jboss-cli.sh --connect\
 --controller=https-remoting://${MGMT_HOST}:${MGMT_PORT}\
 -Djavax.net.ssl.trustStore=${TRUST_STORE}\
 command=/host=${HOST_NAME}/server-config=${SERVER_NAME}:start
exit 0
