#!/usr/bin/python
 
from ansible.module_utils.basic import *
import subprocess
import json

def init(data):
    cmd = data['jboss_home'] + '/bin/jboss-cli.sh'
    exists, created, alreadyCreatedResult,alreadyCreatedErr = isAlreadyDone(data)
    controller = "--controller=https-remoting://%s:%s" % (data['controller_host'],data['controller_port'])
    timeout = "--timeout=%s" % (data['timeout'])    
    user = "-u=%s" % (data['user'])
    password = "-p=%s" % (data['password'])
    if data['trustStorePath'] != None:
      trustStore = "-Djavax.net.ssl.trustStore=%s" % data['trustStorePath']
    else:
      trustStore = "-Dnothing"
    isError = False
    hasChanged = True
    meta = {}
    return cmd,exists,created,alreadyCreatedResult,alreadyCreatedErr,controller,user,password,trustStore,isError,hasChanged,meta,timeout

def checkError(result):
    if "=> \"success\"" in result:
        return False #No error
    else:
        return True #Error

def checkCreated(result):
    if "=> \"success\"" in result:
        return True #Object created
    else:
        return False #Object not created

def makeCall(commandArgs):
    p = subprocess.Popen(commandArgs, stdout=subprocess.PIPE)
    result , err = p.communicate()
    created = False
    remoteExists = False

    if "WFLYPRT0053" in result:
        remoteExists = False
    else:
        remoteExists = True
    created = checkCreated(result)
    return remoteExists, created,result,err

def makeCall1(commandArgs):
    p = subprocess.Popen(commandArgs, stdout=subprocess.PIPE)
    result,err = p.communicate()
    isError = checkError(result)
    return isError,result,err

def isServerGroupAlreadyCreated(data):
    cmd = data['jboss_home'] + '/bin/jboss-cli.sh'
    cli = "/server-group=%s:query" % (data['server_group_name'])
    controller = "--controller=https-remoting://%s:%s" % (data['controller_host'],data['controller_port'])
    user = "-u=%s" % (data['user'])
    password = "-p=%s" % (data['password'])
    if data['trustStorePath'] != None:
      trustStore = "-Djavax.net.ssl.trustStore=%s" % data['trustStorePath']
    else:
      trustStore = "-Dnothing"
    args = ["sh", cmd, "-c", cli, controller, user, password,trustStore]
    return makeCall(args)

def buildResponse(result, err, alreadyCreatedResult, alreadyCreatedErr,hasChanged,isError):
    return {
		"status": "OK", 
		"response": result, 
		"error":err, 
		"checkResponse": alreadyCreatedResult, 
		"checkErr": alreadyCreatedErr,
                "hasChanged": hasChanged,
                "isError": isError
           }

def isAlreadyDone(data):
    cmd = data['jboss_home'] + '/bin/jboss-cli.sh'
    cli = "/server-group=%s:query" % (data['server_group_name'])
    controller = "--controller=https-remoting://%s:%s" % (data['controller_host'],data['controller_port'])
    timeout = "--timeout=%s" % (data['timeout'])
    user = "-u=%s" % (data['user'])
    password = "-p=%s" % (data['password'])
    if data['trustStorePath'] != None:
      trustStore = "-Djavax.net.ssl.trustStore=%s" % data['trustStorePath']
    else:
      trustStore = "-Dnothing"
    args = ["sh", cmd, "-c", cli, controller, user, password,trustStore]
    return makeCall(args)

def server_group_present(data):
    cmd,exists,created,alreadyCreatedResult,alreadyCreatedErr,controller,user,password,trustStore,isError,hasChanged,meta,timeout = init(data)
    if not exists:
        mesg = "Could not connect https-remoting://%s:%s" % (data['controller_host'],data['controller_port'])
        meta = {"status": "Error", "response": mesg}
        isError = True
        hasChanged = False
    else:
        if not created:
            cli = "/server-group=%s:add(profile=%s, socket-binding-group=%s)" % (data['server_group_name'],data['server_group_profile'],data['socket_binding_group'])
            commandArgs = ["sh", cmd, "-c", cli, controller, user, password, trustStore,timeout]
            isError,result,err = makeCall1(commandArgs)            
            if isError == False:
              hasChanged = True
            meta = buildResponse(result, err, alreadyCreatedResult, alreadyCreatedErr,hasChanged,isError)
        else:
            hasChanged = False
            isError = False
            resp = "Server group %s already created" % (data['server_group_name'])
            meta = buildResponse(resp, {}, alreadyCreatedResult, alreadyCreatedErr,hasChanged,isError)
    return isError, hasChanged, meta

def server_group_absent(data):
    cmd,exists,created,controller,alreadyCreatedResult,alreadyCreatedErr,user,password,trustStore,isError,hasChanged, meta,timeout = init(data)
    if not exists:
        mesg = "Could not connect https-remoting://%s:%s" % (data['controller_host'],data['controller_port'])
        meta = {"status": "Error", "response": mesg}
        isError = True
        hasChanged = False
    else:
        if not created:
            hasChanged = False
            isError = True
            resp = "Server group %s does not exist" % (data['server_group_name'])
            meta = buildResponse(resp, {}, alreadyCreatedResult, alreadyCreatedErr,hasChanged, isError)
        else:
            cli = "/server-group=%s:remove" % (data['server_group_name'])
            commandArgs =["sh", cmd, "-c", cli, controller, user, password, trustStore,timeout]
            isError,result,err = makeCall1(commandArgs);
            meta = buildResponse(result, err, alreadyCreatedResult, alreadyCreatedErr,hasChanged,isError)
    return isError, hasChanged, meta

def server_group_start(data):
    cmd,exists,created,alreadyCreatedResult,alreadyCreatedErr,controller,user,password,trustStore,isError,hasChanged, meta,timeout = init(data)
    if not exists:
        mesg = "Could not connect https-remoting://%s:%s" % (data['controller_host'],data['controller_port'])
        meta = {"status": "Error", "response": mesg, "checkResponse": alreadyCreatedResult}
        isError = True
        hasChanged = False
    else:
        if created:
            cli = "/server-group=%s:start-servers" % (data['server_group_name'])
            commandArgs = ["sh", cmd, "-c", cli, controller, user, password, trustStore,timeout]
            isError,result,err = makeCall1(commandArgs);
            meta = buildResponse(result, err, alreadyCreatedResult, alreadyCreatedErr,hasChanged, isError)
        else:
            hasChanged = False
            isError = True
            resp = "Server group %s does not exist" % (data['server_group_name'])
            meta = buildResponse(resp, {}, alreadyCreatedResult, alreadyCreatedErr,hasChanged, isError)
    return isError, hasChanged, meta

def server_group_stop(data):
    cmd,exists,created,alreadyCreatedResult,alreadyCreatedErr,controller,user,password,trustStore,isError,hasChanged, meta,timeout = init(data)
    if not exists:
        mesg = "Could not connect https-remoting://%s:%s" % (data['controller_host'],data['controller_port'])
        meta = {"status": "Error", "response": mesg}
        isError = True
        hasChanged = False
    else:
        if created:
            cli = "/server-group=%s:stop-servers" % (data['server_group_name'])
            commandArgs =["sh", cmd, "-c", cli, controller, user, password, trustStore,timeout]
            isError,result,err = makeCall1(commandArgs);
            meta = buildResponse(result, err, alreadyCreatedResult, alreadyCreatedErr,hasChanged,isError)
        else:
            hasChanged = False
            isError = True
            resp = "Server group %s does not exist" % (data['server_group_name'])
            meta = buildResponse(resp, {}, alreadyCreatedResult, alreadyCreatedErr,hasChanged, isError)
    return isError, hasChanged, meta

def main():
 
    fields = {
        "jboss_home" : {"required": True, "type": "str"},
        "server_group_name": {"required": True, "type": "str"},
        "server_group_profile": {
            "required": False,
            "default": "default",
            "type": "str"
        },
        "socket_binding_group": {
            "required": False,
            "default": "standard-sockets",
            "type": "str"
        },
        "controller_host": {
            "required": False,
            "default": "localhost",
            "type": "str"
        },
        "controller_port": {
            "required": False,
            "default": 9990,
            "type": "int"
        },
        "user" : {
            "required": True,
            "type": "str"
        },
        "password" : {
            "required": True,
            "type": "str"
        },
        "state": {
            "default": "present",
            "choices": ['present', 'absent', 'start', 'stop'],
            "type": 'str'
        },
        "trustStorePath": {
            "required": False,
            "type": 'str',
            "default":"" 
        },
        "timeout": {
            "required": False,
            "type": 'str',
            "default":"30000"
        }
    }
 
    choice_map = {
        "present": server_group_present,
        "absent": server_group_absent,
        "start": server_group_start,
        "stop": server_group_stop
    }
 
    module = AnsibleModule(argument_spec=fields)
    is_error, has_changed, result = choice_map.get(
        module.params['state'])(module.params)
 
    if not is_error:
        module.exit_json(changed=has_changed, meta=result)
    else:
        module.fail_json(msg="Error creating server group", meta=result)

if __name__ == '__main__':
    main()
